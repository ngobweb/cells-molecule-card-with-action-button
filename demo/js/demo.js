var templateBind = document.getElementById('tbind');

// The dom-change event signifies when the template has stamped its DOM.
templateBind.addEventListener('dom-change', function() {
  // auto binding template is ready
  templateBind.set('greeting', 'Try declarative!');
});

document.addEventListener('WebComponentsReady', function() {
  // set component properties here
  var items = [
    {
      type: 'amount',
      description: 'monthly-pay',
      amount: 4444,
      localCurrency: 'MXN',
      currencyCode: 'MXN'
    },
    {
      type: 'amount',
      description: 'remaining',
      amount: 1000,
      localCurrency: 'MXN',
      currencyCode: 'MXN'
    },
    {
      type: 'date',
      description: 'payment-date',
      date: '2016-08-18',
      formatDate: 'LL'
    }
  ];
  var myEL = document.querySelector('#myEl');
  // myEL.set('title', 'Resumen de tus condiciones');
  myEL.set('buttonText', 'Pagar Tarjeta');
  myEL.set('items', items);
});
