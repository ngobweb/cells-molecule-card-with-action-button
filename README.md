# cells-molecule-card-with-action-button

Your component description.

Example:
```html
<cells-molecule-card-with-action-button></cells-molecule-card-with-action-button>
```

```

## Styling

The following custom properties and mixins are available for styling:

| Custom property | Description     | Default        |
|:----------------|:----------------|:--------------:|
| --cells-molecule-card-with-action-button-scope      | scope description | default value  |
| --cells-molecule-card-with-action-button  | empty mixin     | {}             |
