(function() {

  'use strict';

  Polymer({

    is: 'cells-molecule-card-with-action-button',

    behaviors: [
      Polymer.i18nBehavior
    ],

    properties: {
      /*Card title
      * @type: String
      * @public
      */
      title: {
        type: String,
        value: null
      },
      /*Action button text
      * @type: String
      * @public
      */
      buttonText: {
        type: String,
        value: null
      },
      /* Card items, items that are gonna be displayed
      * @type: Object
      * @public
      */
      items: {
        type: Array,
        value: function() {
          return [];
        }
      },
      /*On click button, notify event
      * @type: String
      * @public
      */
      notifyEvent: {
        type: String,
        value: 'on-click-card-button'
      },
      /*Media query breakpoint
      * @type: String
      * @public
      */
      mediaQuery: {
        type: String,
        value: '(max-width: 48rem)'
      },
      /*Is mobile version
      * @type: String
      * @public
      */
      isMobile: {
        type: Boolean,
        value: false
      }
    },
    /**
    * On click button
    */
    onClickButton: function() {
      this.fire(this.notifyEvent);
    },
    /**
    * Checked Value
    */
    _checkedValue: function(str) {
      return Boolean(str);
    },
    /**
    * checked is mobile and return class
    */
    _checkedIsMobile: function(isMobile) {
      return (isMobile) ? 'card--mobile' : '';
    },
    /**
    * Translation for text
    */
    _translate: function(description, lang) {
      return this.t('cells-molecule-card-with-action-button-' + description, lang);
    }
  });
}());
